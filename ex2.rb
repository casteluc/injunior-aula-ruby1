# Função que retorna a divisão de dois valores (que são transformados em
# números do tipo float antes)
def divide_numbers(num1, num2)
    return num1.to_f / num2.to_f
end

# Chama a função que divide os números
puts divide_numbers(6, 90)