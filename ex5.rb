def filter_array(array)
    # Declara o array que será retornado no final
    new_array = []

    # Para cada número do array passado como parametro, é checado se ele é
    # divisível por 3. Se ele for, é adicionado na lista final
    array.each do |num|
        if num % 3 == 0
            new_array = new_array.push(num)
        end
    end

    # Retorna o array final
    return new_array
end

# Chamando a função
print filter_array([3, 6, 7, 8])