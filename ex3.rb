def square_hash(hash)
    # Define o array que será retornado
    array = []

    # Para cada chave do hash passado como parametro é adicionado um novo item na lista
    # que equivale ao valor ao quadrado da key atual
    hash.keys.each do |key|
        array = array.push(hash[key] ** 2)
    end

    # Retorna o array final de números
    return array
end

# Chamando a função
puts square_hash( {:chave1 => 5, :chave2 => 30, :chave3 => 20})