def operate_array(arrays)
    # Variáveis que vão armazenar o valor da soma e multiplicação
    sum = 0
    mult = 1

    # Percorre os elementos do primeiro e segundo arrays, acessando todos os números de
    # todos os arrays
    arrays.each do |array|
        array.each do |num|
            # Atualiza o valor das variáveis finais
            sum = sum + num
            mult = mult * num
        end
    end
    
    # Imprime o resultado na tela
    puts "Soma #{sum}"
    puts "Multiplicação #{mult}"
end

# Definindo o arary que será usado e chamando a função
teste = [[2, 5, 7], [3, 2, 4, 10], [1, 2, 3]]
operate_array(teste)