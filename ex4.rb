def cast_array(array)
    # Declara o array que será retornado
    final_array = []

    # Para cada item do array passado como parametro é adicionado um novo item no
    # array final em forma de string
    array.each do |num|
        final_array = final_array.push(num.to_s)
    end

    # Retorna o array final
    return final_array
end

# Chamando a função
print cast_array([25, 35, 45])